<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'lRZTssfPkfSEsULAOO6wT7Qk1djOXOveJ3BmyQWhFVgbeCKt0Ky7DAWUvSGvbiBRRu8OiVzdlrlWCFUkUdja/w==');
define('SECURE_AUTH_KEY',  'yN7JJwV3scWHWMPg9syBxjhJ1uKuubVFu5LtNiNt5Z4I3MOBCjh42l59LzKz/fTfpYW6qghYOs+wqukrJnrwVA==');
define('LOGGED_IN_KEY',    '5B/RZqmEdZEwlXy1GJr0F1x7ms9aLl8Tc9WqlEuSuNr8f3/n/gIZZ901H/Q7Teju1NwSxxDJsG1rucSRuQALRQ==');
define('NONCE_KEY',        'olxMxktvwyDDdILlNryVsqlr4t5UYMQ2C/a+lQKLIQBaP/WFv40L4gY0uTyZGp/zX6ePqRft9sFizmnEt1CzsQ==');
define('AUTH_SALT',        'UjcMMu7BTbpNWws0yzXy61sedj31auylQLVryjIhaTcVyojJIMrTmWJ3Az4fP7o5lFZxBiugEy6zc07f/WDr8g==');
define('SECURE_AUTH_SALT', '6X4z3r3uK6msoHP/gar6XU7An16kJOppZr9YMjRg0Qa1SsnYHBuC+MnwUrEMpjZES4vsdlDZRHawEHryBC4zWQ==');
define('LOGGED_IN_SALT',   'lMVdZE6Im+HqnGx43lRx/goJtVQUBdBRF25hZd7DdIwFPVVQPuIi9oN5m8cIjZq/xdybbKNlOD7r8ntW+xevcA==');
define('NONCE_SALT',       'lyGYtd7PuonMP0sIBo52pOb2J+PMwthf/2acysBNDGVfjKb6XpIGwFYdTQmB1V7LW+gNdYfE7ZKy0o29BM1qDA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';





/* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}

/* Inserted by Local by Flywheel. Fixes $is_nginx global for rewrites. */
if (strpos($_SERVER['SERVER_SOFTWARE'], 'Flywheel/') !== false) {
	$_SERVER['SERVER_SOFTWARE'] = 'nginx/1.10.1';
}
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
