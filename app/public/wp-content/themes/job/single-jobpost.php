<?php get_header();

  while(have_posts()) {
    the_post();
     ?>

<div class="jobdescription">

		<div class="jobdescription__banner">

			<div class="wrapper--smaller">
				<div class="row">
					<div class="row__col-8">
						<div class="headline--title"><?php the_title(); ?></div>
						<div class="headline--subtitle">The series group.</div>
						<div class="headline--grey">
							<span class="jobdescription--main">Remotely</span>
							<span class="jobdescription--main">Full time</span>
							<span class="jobdescription--main">Job open</span>
						</div>
					</div>
					<div class="row__col-4">
						<div class="jobdescription__apply"> <button>Apply Now</button> </div>
					</div>
				</div>
			</div>
		</div>



		<div class="wrapper--smaller">
			<div class="row">
				<div class="row__col-8">
					<div class="jobdescription__content">
			<?php the_content(); ?>
		</div>
	</div>
				</div>
			</div>
			
			


</div>



<?php }
  get_footer(); ?>