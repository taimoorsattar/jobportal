<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <?php wp_head(); ?>
</head>
<body class="btn--backblack">
  <header class="site-header">
    <div class="wrapper">
      <a href="<?php echo site_url() ?>">
        <div class="site-header__logo">
          Jobee
        </div>
      </a>

      <div class="site-header__menu-icon">
        <div class="site-header__menu-icon__middle"></div>
      </div>

      <div class="site-header__menu-content">
        <div class="site-header__btn-container">
          <a href="<?php echo esc_url(site_url('/job-post')); ?>" class="btn btn--green">Post a job</a>
        </div>

        <nav class="primary-nav primary-nav--pull-right">
          <ul>
            <?php if(is_user_logged_in()) { ?>
            <li><a href="<?php echo wp_logout_url(); ?>">Log out</a></li>
            <li><a href="blog.html">Blog</a></li>
            <?php } else { ?>

            <li><a href="<?php echo wp_registration_url(); ?>">Sign Up</a></li>
            <li><a href="<?php echo wp_login_url(); ?>">Login</a></li>
            <li><a href="blog.html">Blog</a></li>
          </ul>
          <?php } ?>
        </nav>
      </div>
      
    </div>

  </header>