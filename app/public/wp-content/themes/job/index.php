<?php get_header(); ?>

<div class="site-banner">
    <h1 class="headline headline__banner headline--shadow">Hire the qualified person in one place..</h1>
    <div class="site-banner__search-box">
      <h3>Find what you are looking for</h3>
      <input class="site-banner__input" type="search" placeholder="Your search here..."><input class="site-banner__input-submit" type="submit" value="Search">
    </div>
  </div>


  <div class="jobPost">
    <div class="wrapper">
      <div class="headline--big">
        <span>Job board</span>
        <span class="headline--basicsmaller btn--display-none-medium"><a href="">Post one.</a></span>
        <hr>
      </div>
    </div>

    <?php
      $today= date('Ymd');

      $jobpostblock=new WP_Query(array(
        'posts_per_page' => 2,
        'post_type'=>'jobpost'
      ));

      while($jobpostblock->have_posts()){
        
        $jobpostblock->the_post();
        get_template_part('template-parts/content', 'jobblock');
      }
      ?>

  </div>

<?php get_footer(); ?>