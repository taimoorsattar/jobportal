<div class="jobPost__section">
  <a href="<?php the_permalink(); ?>">
    <div class="row">
      <div class="row__col-8 row__col-med-12">
        <h1 class="jobPost__jobname"><?php the_title(); ?><span class="jobPost__duration">Remotely</span></h1>
      </div>

      <div class="row__col-4">
        <span class="jobPost__time">12 minute ago.</span>
      </div>
    </div>

    <h2 class=" jobPost__companyname headline--lightbold">The series group.</h2>
  </a>
</div>