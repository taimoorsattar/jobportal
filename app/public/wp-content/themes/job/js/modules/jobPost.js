import $ from 'jquery';

class jobPost {
  constructor() {
    this.events();
  }

  events() {
    $(".post-submit").on("click", this.createJobBlock.bind(this));
  }

  
  createJobBlock(e) {
    console.log('got it');
    var ourNewPost = {
      'title': $(".post-title").val(),
      'content': $(".post-job-description").val(),
      'status': 'publish'
    }
    
    $.ajax({

      beforeSend: (xhr) => {
        xhr.setRequestHeader('X-WP-Nonce', portalData.nonce);
      },
      url: portalData.root_url + '/wp-json/wp/v2/jobpost/',
      type: 'POST',
      data: ourNewPost,
      success: (response) => {
        console.log("Congrats");
        console.log(response);
      },
      error: (response) => {
        console.log("Sorry");
        console.log(response);
      }
    });
  }
}

export default jobPost;