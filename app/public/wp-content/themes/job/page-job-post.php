



<?php 
  if (!is_user_logged_in()) {
    wp_redirect(esc_url(site_url('/wp-login.php')));
    exit;
  }
  get_header();
  while(have_posts()) {
    the_post();
     ?>



  <div class="wrapper--smaller">
    <form class="postfield">
      <h2 class="postfield__maintitle">About the Job.</h2>

      <div class="postfield--gap">
<label for="jobposition">Job Position:</label>


        <input class="postfield__input post-title" type="text" id="jobposition" name="jobposition" placeholder="e.g. 'Web developer'">
      </div>


      <div class="postfield--gap">
        <label for="catagoryjob">Select job catagory:</label>
        <select name="catagoryjob" id="catagoryjob">
          <option value="webdev">Web developer</option>
          <option value="graphicdesigner">Graphic designer</option>
          <option value="Seo">Market and Seo</option>
          <option value="other">Other</option>
        </select>
      </div>
      


      <div class="postfield--gap">
        <fieldset>
          <legend>Select job type:</legend>
          <input type="radio" name="time" id="parttime"><label for="parttime">Part Time</label>
          <input type="radio" name="time" id="fulltime"><label for="fulltime">Full Time</label>
        </fieldset>
      </div>


      <div class="postfield--gap">
        <label for="jobdescription">Enter about job here:</label>
        <br>
        <textarea class="postfield__textarea post-job-description" name="jobdescription" id="jobdescription"></textarea>
      </div>



        <h2 class="postfield__maintitle">About the Company.</h2>

        <div class="postfield--gap">
          <label for="companyname">Company name:</label>
          <input class="postfield__input" type="text" id="companyname" name="companyname">
        </div>
        

        <div class="postfield--gap">
          <label for="CompanyURL">Company Website:</label>
          <input class="postfield__input" type="text" id="CompanyURL" name="CompanyURL">
        </div>

        <div class="postfield--gap">
          <label for="companyMessage">Company details:</label>
          <br>
          <textarea class="postfield__textarea" name="companyMessage" id="companyMessage"></textarea>
        </div>


        <div class="postfield--gap">
          <div class="postfield__submit post-submit">Submit</div>
        </div>
        
      </form>
    </div>
  <?php }

  get_footer(); ?>