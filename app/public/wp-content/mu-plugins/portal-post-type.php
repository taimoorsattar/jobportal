<?php

function portal_post_types() {
  // Campus Post type
  register_post_type('jobpost', array(
    'supports' => array('title', 'editor', 'excerpt'),
    'rewrite' => array('slug' => 'jobpostes'),
    'show_in_rest' => true,
    'has_archive' => true,
    'public' => true,
    'labels' => array(
      'name' => 'job Posts',
      'add_new_item' => 'Add New job post',
      'edit_item' => 'Edit job post',
      'all_items' => 'All job postes',
      'singular_name' => 'jobpost'
    ),
    'menu_icon' => 'dashicons-hammer'
  ));
}

add_action('init', 'portal_post_types');